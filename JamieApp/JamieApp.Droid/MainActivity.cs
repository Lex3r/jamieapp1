﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Gms.Ads;

namespace JamieApp.Droid
{
    [Activity(Label = "JamieApp", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            MobileAds.Initialize(ApplicationContext, "ca-app-pub-7393334420865995~4695994663");

            LoadApplication(new App());
        }
    }
}

