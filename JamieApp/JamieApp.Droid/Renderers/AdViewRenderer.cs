using Android.Widget;
using Android.Gms.Ads;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(JamieApp.Controls.AdView), typeof(JamieApp.Droid.PlatformSpecific.AdViewRenderer))]
namespace JamieApp.Droid.PlatformSpecific
{
    public class AdViewRenderer : ViewRenderer<Controls.AdView, AdView>
    {
        private string adUnitId = string.Empty;
        private AdSize adSize = AdSize.SmartBanner;
        private AdView adView;

        protected override AdView CreateNativeControl()
        {
            if (adView != null)
                return adView;

            adUnitId = Forms.Context.Resources.GetString(Resource.String.banner_ad_unit_id);
            adView = new AdView(Forms.Context);
            adView.AdSize = adSize;
            adView.AdUnitId = adUnitId;

            var adParams = new LinearLayout.LayoutParams(LayoutParams.WrapContent, LayoutParams.WrapContent);

            adView.LayoutParameters = adParams;

            adView.LoadAd(new AdRequest
                            .Builder()
                            .Build());
            return adView;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Controls.AdView> e)
        {
            base.OnElementChanged(e);

            if (Control == null) {
                CreateNativeControl();
                SetNativeControl(adView);
            }
        }
    }
}