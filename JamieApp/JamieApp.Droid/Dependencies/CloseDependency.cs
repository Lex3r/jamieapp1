using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using JamieApp.Droid.Dependencies;
using JamieApp.Contracts;

[assembly: Xamarin.Forms.Dependency(typeof(CloseDependency))]
namespace JamieApp.Droid.Dependencies
{
    public class CloseDependency : ICloseApp
    {
        public void CloseApp()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }
    }
}