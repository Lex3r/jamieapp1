﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace JamieApp.MarkupExtensions
{
    [ContentProperty("ResourceId")]
    public class EmbededImage : IMarkupExtension
    {
        public string ResourceId { get; set; }

        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (string.IsNullOrWhiteSpace(ResourceId))
                return null;

            return ImageSource.FromResource(ResourceId);
        }
    }
}
