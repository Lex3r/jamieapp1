﻿using System.Collections.Generic;

namespace JamieApp
{
    public class MenuItems
    {
        private static List<MenuItem> items;
        public static List<MenuItem> Items
        {
            get
            {
                if (instance == null) {
                    instance = new MenuItems();
                }
                return items;
            }
        }

        public static MenuItems instance;

        public MenuItems()
        {
            items = new List<MenuItem>();


            //ADD YOUR ITEMS HERE

            AddItem("Title1", "1.html");
            AddItem("Title2", "2.html");
            AddItem("Title3", "3.html");
        }

        private void AddItem(string title, string fileName)
        {
            items.Add(new MenuItem { Title = title, FileName = fileName });
        }

        public class MenuItem
        {
            public string Title { get; set; }

            public string FileName { get; set; }
        }
    }
}
