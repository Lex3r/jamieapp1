﻿using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using JamieApp.Contracts;

namespace JamieApp
{
    public partial class HtmlContentPage : ContentPage, INotifyPropertyChanged
    {
        public HtmlContentPage(string fileName)
        {
            InitializeComponent();

            Task.Run(async () => {
                activityIndicator.IsRunning = true;

                var webViewSource = new HtmlWebViewSource {
                    Html = GetHtmlText(fileName)
                };

                webView.Source = webViewSource;

                await Task.Delay(1000);

                activityIndicator.IsRunning = false;
            });
        }

        private string GetHtmlText(string fileName)
        {
            string text = "";

            var assembly = typeof(HtmlContentPage).GetTypeInfo().Assembly;

            using (var stream = assembly.GetManifestResourceStream($"JamieApp.ContentHtml.{fileName}")) {
                using (var reader = new StreamReader(stream)) {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }

        protected override bool OnBackButtonPressed()
        {
            if (Device.OS == TargetPlatform.Android) {
                Device.BeginInvokeOnMainThread(async () => {
                    var result = await DisplayAlert("Warning", "Are you sure you want to exit the app?", "Yes", "No");
                    if (result)
                        DependencyService.Get<ICloseApp>().CloseApp();
                });
            }

            return true;
        }       
    }
}
