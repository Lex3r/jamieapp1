﻿using System.Linq;
using Xamarin.Forms;

namespace JamieApp
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();

            listView.ItemsSource = MenuItems.Items;

            listView.SelectedItem = MenuItems.Items.First();

            IsPresented = false;
        }


        private void Handle_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var menuItem = e.SelectedItem as MenuItems.MenuItem;
            Detail = new NavigationPage(new HtmlContentPage(menuItem.FileName));

            IsPresented = false;
        }
    }
}
